+++
title = "Mes projets en cours"
date = 2020-08-13T10:37:27+02:00
draft = false
author = "Romain Chardiny"
authorTwitter = "romch007" #do not include @
cover = ""
description = "Si vous voulez en savoir plus sur ce que je fait"
tags = ["projet", "projets"]
keywords = ["k8s", "nodejs", "projet", "php", "html", "css"]
showFullContent = false
+++

## Projets en cours

Je travaille actuellement sur un projet nommé Topic App qui permet aux étudiants d'exprimer leur voix à travers des articles, des événements et des pétitions, sur une application.

Si vous êtes intéressé, vous pouvez visiter le [site web](https://beta.topicapp.fr) ou [la page Twitter](https://twitter.com/topic_app).

Je m'occupe de la partie serveur, qui est écrite en Javascript. Je m'occupe aussi de la partie déploiement sur un petit cluster Swarm.

## Anciens projets

J'ai aussi réalisé des petits projets, surtout des sites web, durant mes stages ou pour des devoirs :

- [Site web](https://stage.rom.chardiny.net) créé durant mon stage de 3ème
  (à 13 ans) chez [Aepsilon](https://aepsilon.com/). Fait avec HTML/CSS et PHP.

  C'est une sorte de blog qui affiche le contenu de mon rapport de stage. J'ai perdu les données donc il est peut-être vide.

- [Site web](https://svt.rom.chardiny.net) créé pour un devoir de SVT en 2nde. Fait avec HTML/CSS et Node.js/EJS.

Ce blog et ces sites web sont déployés sur un cluster Kubernetes fait maison et maintenu par mon père (j'utilise un seul namespace).

Visitez mon [GitLab](https://gitlab.com/romch007).
