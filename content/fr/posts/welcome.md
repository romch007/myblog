+++
title = "Bienvenue sur mon blog !"
date = "2020-08-06"
author = "Romain Chardiny"
authorTwitter = "romch007" #do not include @
cover = ""
tags = ["bienvenu"]
keywords = ["", ""]
description = ""
showFullContent = false
+++

# À propos de moi

Je suis un étudiant français interessé par l'informatique, surtout par les technologies backend.

J'ai découvert la programmation en créant des petits jeux sur Scratch.

Mes principaux intérêts :

- Dev: Node.js, Golang, Python

- Ops: Docker, Swarm, Kubernetes

- Base de données: PostgreSQL, MongoDB
