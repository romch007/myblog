---
title: "À propos"
date: 2020-08-02T18:19:59+02:00
draft: false
---

Ce blog appartient à Romain Chardiny.

Liens :

- [Twitter](https://twitter.com/romch007)

- [LinkedIn](https://www.linkedin.com/in/romain-chardiny-38a15a1b4/)

- [GitLab](https://gitlab.com/romch007)
