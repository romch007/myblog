---
title: "About"
date: 2020-08-02T18:19:59+02:00
draft: false
---

This blog belongs to Romain Chardiny.

Links:

- [Twitter](https://twitter.com/romch007)

- [LinkedIn](https://www.linkedin.com/in/romain-chardiny-38a15a1b4/)

- [GitLab](https://gitlab.com/romch007)
