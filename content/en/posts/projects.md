+++
title = "My current projects"
date = 2020-08-13T10:37:27+02:00
draft = false
author = "Romain Chardiny"
authorTwitter = "romch007" #do not include @
cover = ""
description = "If you want to learn more about my projects"
tags = ["project", "projects"]
keywords = ["k8s", "nodejs", "projects", "php", "html", "css"]
showFullContent = false
+++

## Current projects

I'm currently working on a project named Topic App which allow student express
their voice through articles, events and petitions, on an app.

If you are interested in, go to [the website](https://beta.topicapp.fr) or [Twitter](https://twitter.com/topic_app).

I take care of the backend part, which is written in Javascript. I also take care
of the deployment part on a tiny Swarm cluster. [DevOps :)]

## Previous projects

I also created small projects, mainly websites, during my internships or for
school homework:

- [Website](https://stage.rom.chardiny.net) (in French) created during my third
  year (13 y/o) internship at [Aepsilon](https://aepsilon.com/). Made
  with HTML/CSS and PHP.

  It is a kind of blog
  which shows the content of my internship report. I lost the content once so
  it might be empty;

- [Website](https://svt.rom.chardiny.net) (in French) created for a SVT
  homework. Make with HTML/CSS and Node.js/EJS.

This blog and those websites are deployed on a homemade Kubernetes cluster,
maintained by my dad (I own a single namespace).

Visit my [GitLab](https://gitlab.com/romch007).
