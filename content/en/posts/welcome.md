+++
title = "Welcome to by blog!"
date = "2020-08-06"
author = "Romain Chardiny"
authorTwitter = "romch007" #do not include @
cover = ""
tags = ["welcome"]
keywords = ["", ""]
description = ""
showFullContent = false
+++

# About me

I'm a French student interested in computer science, especially backend technologies.

I discover programming by creating little games with Scratch.

My main interests :

- Dev: Node.js, Golang, Python

- Ops: Docker, Swarm, Kubernetes

- Databases: PostgreSQL, MongoDB
